// Define an array to store information
var informationArray = [];
// Example usage
product = {
  title: "Product 1",
  description: "Description",
  prix: "250",
  quantité: "2500",
};

// Function to add information to the array
function addInformation(newInfo) {
  informationArray.push(newInfo);
  return "Added!";
}

addInformation(product);

// Display the array
console.log(informationArray);
